﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;

namespace XamarinZ
{
    public class NotesItemDatabase
    {
        readonly SQLiteAsyncConnection database;

        
        public NotesItemDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection (dbPath);
            database.CreateTableAsync<NotesItem>().Wait();
        }

        public Task<List<NotesItem>> GetItemsAsync()
        {
            return database.Table<NotesItem>().ToListAsync();
        }

        public Task<List<NotesItem>> GetItemsNotDoneAsync()
        {
            return database.QueryAsync<NotesItem>("SELECT * FROM [NotesItem] WHERE [Done] = 0");
        }

        public Task<NotesItem> GetItemAsync(int id)
        {
            return database.Table<NotesItem>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(NotesItem item)
        {
            if (item.ID != 0)
            {
                return database.UpdateAsync(item);
            }
            else
            {
                
                 return database.InsertAsync(item);   
            }
        }

        
        public Task<int> DeleteItemAsync(NotesItem item)
        {
            return database.DeleteAsync(item);
        }


    }
}