﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace XamarinZ
{
    public class UserTable
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ProfileImg { get; set; }
    }
}
