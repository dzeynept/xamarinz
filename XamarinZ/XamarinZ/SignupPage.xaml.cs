﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinZ
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}
        public async void SignUp(object sender, EventArgs e)
        {
            string username = UsernameEntry.Text;
            // signup 
            BindingContext = new UserTable();

            var user = (UserTable)BindingContext;
            await App.Userdatabase.SaveItemAsync(user);
        }
        public async void NavigateBtn(object sender, EventArgs e)
        {
            

            await Navigation.PushAsync(new LoginPage());
        }
    }
}
