﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinZ
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NotesPage : ContentPage
	{
      
		public NotesPage ()
		{
			InitializeComponent ();
        }
       
     
   
        async void OnSaveClicked(object sender, EventArgs e)
        {
            var noteItem = (NotesItem)BindingContext;
      
            await App.Database.SaveItemAsync(noteItem);
          //  await Navigation.PopAsync();
        }
        async void OnDeleteClicked(object sender, EventArgs e)
        {
            var todoItem = (NotesItem)BindingContext;
            await App.Database.DeleteItemAsync(todoItem);
            await Navigation.PopAsync();
        }





    }
        
}