﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace XamarinZ
{
    public class NotesItem
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
    }

}