﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace XamarinZ
{
	public class UserItemPageCS : ContentPage
	{
		public UserItemPageCS ()
		{
            Title = "Todo Item";
            
            var usernamesEntry = new Entry();
            usernamesEntry.SetBinding(Entry.TextProperty, "Name");
            var passwordEntry = new Entry();
            passwordEntry.SetBinding(Entry.TextProperty, "Password");
            var signupBtn = new Button { Text = "Sign Up" };
            signupBtn.Clicked += async (sender, e) =>
            {
                var user = (UserTable)BindingContext;
                await App.Userdatabase.SaveItemAsync(user);
                await Navigation.PopAsync();
            };

            var loginBtn = new Button { Text = "Login" };
            loginBtn.Clicked += async (sender, e) =>
            {
                
            };



            Content = new StackLayout
            {
                Margin = new Thickness(20),
                VerticalOptions = LayoutOptions.StartAndExpand,
                Children =
                {

                    new Label { Text = "Name"},
                    usernamesEntry,
                    new Label { Text = "Password"},
                    passwordEntry,
                    signupBtn,
                    loginBtn
                }
            };
        }
	}
}