﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;

namespace XamarinZ
{
    public class UserDatabase
    {
        readonly SQLiteAsyncConnection userdatabase;
        public UserDatabase(string dbPath)
        {
            userdatabase = new SQLiteAsyncConnection(dbPath);
            userdatabase.CreateTableAsync<UserTable>().Wait();
        }

        public Task<List<UserTable>> GetItemsAsync()
        {
            return userdatabase.Table<UserTable>().ToListAsync();
        }

        public Task<List<UserTable>> GetItemsNotDoneAsync()
        {
            return userdatabase.QueryAsync<UserTable>("SELECT * FROM [NotesItem] WHERE [Done] = 0");
        }

        public Task<UserTable> GetItemAsync(int id)
        {
            return userdatabase.Table<UserTable>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(UserTable item)
        {
            if (item.ID != 0)
            {
                return userdatabase.UpdateAsync(item);
            }
            else
            {
                return userdatabase.InsertAsync(item);
            }
        }
        public Task<int> DeleteItemAsync(UserTable item)
        {
            return userdatabase.DeleteAsync(item);
        }
    }
}
