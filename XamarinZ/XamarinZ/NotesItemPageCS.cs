﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace XamarinZ
{
	public class NotesItemPageCS : ContentPage
	{
		public NotesItemPageCS ()
		{
            Title = "Todo Item";

            

            var notesEntry = new Entry();
            notesEntry.SetBinding(Entry.TextProperty, "Notes");
            var saveButton = new Button { Text = "Save" };
            saveButton.Clicked += async (sender, e) =>
            {
                var todoItem = (NotesItem)BindingContext;
                await App.Database.SaveItemAsync(todoItem);
                await Navigation.PopAsync();
            };

            var deleteButton = new Button { Text = "Delete" };
            deleteButton.Clicked += async (sender, e) =>
            {
                var todoItem = (NotesItem)BindingContext;
                await App.Database.DeleteItemAsync(todoItem);
                await Navigation.PopAsync();
            };

            

            Content = new StackLayout
            {
                Margin = new Thickness(20),
                VerticalOptions = LayoutOptions.StartAndExpand,
                Children =
                {
                    
                    new Label { Text = "Notes" },
                    notesEntry,
                    saveButton,
                    deleteButton
                }
            };
        }
	}
}