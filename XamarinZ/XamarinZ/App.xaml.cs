using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.IO;


[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace XamarinZ
{
	public partial class App : Application
	{
        static NotesItemDatabase database;
        static UserDatabase userdatabase;
        public App ()
		{
			InitializeComponent();

            MainPage = new NavigationPage(new MainPage());


        }
        public static NotesItemDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new NotesItemDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TodoSQLite.db3"));
                }
                return database;
            }
        }

        public int ResumeAtTodoId { get; set; }

        public static UserDatabase Userdatabase
        {
            get
            {
                if (userdatabase == null)
                {
                    userdatabase = new UserDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TodoSQLite.db3"));
                }
                return userdatabase;
            }
        }

        public int ResumeAtUserId { get; set; }
        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
